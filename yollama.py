import sublime
import sublime_plugin

import requests
import json

contexts = {}  # Only R/W from ST commands!


# --- Ask anything ---


class YollamaAskAnyCommand(sublime_plugin.WindowCommand):
    def run(self, **kw):
        prompt = kw["prompt"]
        window = self.window
        view = get_output_panel_for_window(window)
        context = get_context_for_view(view)
        
        show_and_scroll_output_panel(window, view)
        pending_status_all(window)
        sublime.set_timeout_async(lambda: generate_streaming_to_output_panel(self.window, prompt, context))

    def input(*args):
        return YollamaPromptInput()


# --- Ask about current file or selection


class YollamaAskAboutCommand(sublime_plugin.TextCommand):
    def run(self, edit, syntax_sensitive=False, **kw):
        view = self.view
        window = view.window()
        prompt = kw["prompt"]
        # Not sure the context helps currently.
        # context = get_context_for_view(view)
        context = None

        sel = view.sel()
        region = None

        if len(sel) == 1:
            for region0 in sel:
                if len(region0) == 0:
                    region = sublime.Region(0, view.size())
                else:
                    region = region0
                break
        else:
            error_status_single_region()
            return

        prompt = prompt + "\n" + view.substr(region)
        show_and_scroll_output_panel(window, view)
        pending_status(view)
        sublime.set_timeout_async(lambda: generate_streaming_to_output_panel(window, prompt, context))

    def input(*args):
        return YollamaPromptInput()


# --- Ask any/about shared classes ---


class YollamaPromptInput(sublime_plugin.TextInputHandler):
    def name(_):
        return "prompt"

    def placeholder(_):
        return "Prompt"


class YollamaInitialResponseCommand(sublime_plugin.TextCommand):
    def run(self, edit, **kw):
        view = self.view
        window = view.window()
        prompt = kw["prompt"]
        response = kw["response"]
        model = kw["model"]
        clear_status(view)
        show_and_scroll_output_panel(window, view)
        view.insert(edit, view.size(), "You: " + prompt +
                        "\n\n" + model + ": " + response)


class YollamaContinuedResponseCommand(sublime_plugin.TextCommand):
    def run(self, edit, **kw):
        view = self.view
        response = kw["response"]
        view.insert(edit, view.size(), response)


class YollamaDoneResponseCommand(sublime_plugin.TextCommand):
    def run(self, edit, **kw):
        view = self.view
        response = kw["response"]
        context = kw["context"]
        clear_status(view) # just making sure
        view.insert(edit, view.size(), response + "\n\n")
        set_context_for_view(view, context)


# --- Code Generation/Infill Completion ---


class YollamaCompletionCommand(sublime_plugin.TextCommand):
    def run(self, edit, syntax_sensitive=False):
        view = self.view
        # Not sure the context helps currently.
        # context = get_context_for_view(view)
        context = None
        sel = view.sel()
        if len(sel) != 1:
            error_status_single_region()
            return

        for region in sel:
            # When it's just the cursor, we try to insert completion in place with in-fill mode
            infill = region.begin() == region.end()
            prompt = ""
            if infill:
                settings = sublime.load_settings("Yollama.sublime-settings")
                pre = view.substr(sublime.Region(0, region.end())) # include region to be replaced (ie if it's a comment)
                suf = view.substr(sublime.Region(region.end(), view.size()))
                prompt = settings.get("infill_prompt_template").format(prefix=pre, suffix=suf)
            else: 
                # If it's a selection, we use codegen mode and just send the selection as prompt
                prompt = view.substr(region)

            pending_status(view, ("infill" if infill else "codegen"))
            sublime.set_timeout_async(lambda: generate_replace_region(view, region, infill, prompt, context))
            # Only a single selection allowed currently; provided the guard condition above this should not be necessary,
            # just a precaution.   
            break


class YollamaReplaceRegionCommand(sublime_plugin.TextCommand):
    def run(self, edit, **kw):
        view = self.view
        response = kw["response"]
        context = kw["context"]
        region = sublime.Region(kw["region_begin"], kw["region_end"])
        clear_status(view)
        view.replace(edit, region, response)
        # Not sure the context helps currently.
        # set_context_for_view(view, context)


# --- Client ---


def _generate_request(view, prompt, context=None, model_key="model", stream=True):
    try:
        settings = sublime.load_settings("Yollama.sublime-settings")
        model = settings.get(model_key)
        url = settings.get("url")
        payload = {
            "model": model,
            "prompt": prompt,
            "stream": stream
        }
        if context is not None:
            payload["context"] = context
    
        req = requests.post(url, json=payload, stream=stream)
        if req.status_code == requests.codes.ok:
            return (req, model)
        else:
            error_status_and_print("Unexpected Ollama status code: " + req.status_code, view=view)
            return (None, None)
    except Exception as e:
        error_status_and_print(e, view=view)
        return (None, None)


def _get_response(body, stream_done=False, view=None):
    if "response" not in body:
        error_status_and_print("Key response not in body", view=view)
        return None

    response = body["response"]
    if not stream_done and len(response.strip()) == 0:
        error_empty_status(view=view)
        return None

    return response


def generate_streaming_to_output_panel(window, prompt, context):
    view = get_output_panel_for_window(window)
    req, model = _generate_request(view, prompt, context=context)
    if req is None:
        return

    initial = True    
    for line in req.iter_lines():
        if line:
            decoded_line = line.decode('utf-8')
            token = json.loads(decoded_line)

            done = False
            if "done" in token:
                done = token["done"]
            response = _get_response(token, stream_done=done, view=view)
            if response is None:
                break
            if "context" in token:
                context = token["context"]

            if initial:
                view.run_command("yollama_initial_response", args={
                    "prompt": prompt,
                    "response": response,
                    "model": model
                })
                initial = False
            else:
                view.run_command("yollama_continued_response", args={
                    "response": response,
                })
            if done:
                view.run_command("yollama_done_response", args={
                    "response": response,
                    "context": context,
                })
                return

    error_status_and_print("Streaming ended before done.", view=view)
    view.run_command("yollama_done_response", args={
        "response": "",
        "context": context,
    })


def generate_replace_region(view, region, infill, prompt, context):
    model_key = "infill_model" if infill else "codegen_model"
    req, _ = _generate_request(view, prompt, context=context, model_key=model_key, stream=False)
    if req is None:
        return
    body = req.json()
    
    response = _get_response(body, view=view)
    if response is None:
        return
    context = None
    if "context" in body:
        context = body["context"]
    
    view.run_command("yollama_replace_region", args={
        "region_begin": region.end(),
        "region_end": region.end(),
        "response": response,
        "context": context,
    })


# --- Utilities ---


def get_output_panel_for_window(window):
    view = window.find_output_panel("yollama")
    if view is None:
        view = window.create_output_panel("yollama")
    return view


def show_and_scroll_output_panel(window, view):
    if window is None or view is None:
        return
    window.run_command('show_panel', {'panel': 'output.yollama'})
    view.show(view.size())


def get_context_for_view(view):
    context_key = "view_" + str(view.id())
    if context_key in contexts:
        return contexts[context_key]
    else:
        return None


def set_context_for_view(view, context):
    if context is not None:
        context_key = "view_" + str(view.id())
        contexts[context_key] = context


def pending_status(view, mode=None):
    status = "Yollama: Sending prompt to Ollama, waiting for a response..." if mode is None else "Yollama: Sending prompt to Ollama (" + mode + "), waiting for a response..."
    view.set_status("yollama", status)


def pending_status_all(window):
    for view in window.views():
        pending_status(view)


def clear_status(view):
    view.erase_status("yollama")
    if view.window() is not None:
        for view0 in view.window().views():
            view0.erase_status("yollama")


def error_status_single_region():
    sublime.status_message("Yollama: Only 1 selected region/cursor is currently supported.")


def error_empty_status(view=None):
    if view is not None:
        clear_status(view)
    sublime.status_message("Yollama: Empty response text from server.")


def error_status_and_print(error, view=None):
    if view is not None:
        clear_status(view)
    sublime.status_message("Yollama: an error occurred, please check console for details.")    
    print("Yollama ", error)
